% PART D
clear all
%definition of variables 
v_e = 1080;
a = (37.5/2)/9;
g = 9.81;
m_0 = 37.5;
k = 1.4;

%solving the differential equation of rockets velocity with propulsion
syms v_i(t)
v_i(t) = dsolve (diff(v_i)+ (k*v_i)/(m_0 -a*t)==((v_e*a)/(m_0 - a*t) - g), v_i(0) == 0);

%equation of the rocket's velocity when fuel depletes
syms v_f(t)
v_f(t) = dsolve (diff(v_f) == - g - ((k*v_f)/(m_0/2)), v_f(9) == v_i(9));

%solving for position of the rocket with propulsion
syms s_i(t)
s_i(t) = int(v_i,t);
s_i(t) = s_i(t) - s_i(0); %subtract the y-int

%solving for position of the rocket after fuel depletes
syms s_f(t)
s_f(t) = int(v_f,t);
s_f(t) = s_f(t) - s_f(9) + s_i(9); % transfomation of position graph to the end point
                                    % of the previous final position

%solving for the time when the rocket reaches the ground
t_r = solve(s_f,t);
double(t_r)

%plotting velocity time graph the rocket
figure(1)
plot(0:0.1:9,v_i(0:0.1:9),'r')
hold on
%plotting the velocity after propulsion stops
plot(9:0.1:t_r,v_f(9:0.1:t_r),'g')
%labeling the graph
set(gca,'FontSize',12);
title('Velocity of Rocket vs Time')
ylabel('Velocity (m/s)')
xlabel('Time (s)')
legend('With propulsion','No propulsion')
grid on
grid minor
box off

%plotting position time graph of the rocket
figure(2)
plot(0:0.1:9,s_i(0:0.1:9),'r')
hold on
%plotting the position after propulsion stops
plot(9:0.1:t_r,s_f(9:0.1:t_r),'g')
%labeling the graph
set(gca,'FontSize',12);
title('Height of Rocket vs Time')
ylabel('Height (m)')
xlabel('Time (s)')
legend('With propulsion','No propulsion')
grid on
grid minor
box off

%solving for time at max height
t_max = solve(v_f,t);
double(t_max)

%solving for max height using the time at max height
h_max = s_f(t_max);
double(h_max)

%solving for terminal velocity
double(v_f(t_r))
