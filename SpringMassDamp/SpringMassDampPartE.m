clear all;
% Denifition of Variables
F0 = 1;
m = 1;
Wn = 2*pi;
T = [1, 0.5];

%plottting the Equation from Part D with diffrent periods 
syms n;
for i = 1:2 
    syms x(t)
    Dx = diff(x);
    x(t) = F0/(m*Wn) * symsum(sin(Wn*(t-n*T(i)))*heaviside(t-n*T(i)), n, 1, inf);
    %plotting spring-mass-damper system response
    figure(2)
    set(gca,'FontSize',12);
    plot(0:0.1:10,x(0:0.1:10));
    hold on
    clear x(t)
end

%labeling the graph
title('Spring Mass Damper Response System Displacement vs Time')
ylabel('Displacement (m)')
xlabel('Time (s)')
legend('T = 1','T = 0.5','Location','southwest')
box off 
